### Video Splitter for Windows Phone 8.1 or higher ###

* A Video Splitter app made for WhatsApp. This app compensates the inability of WhatsApp app on Windows Phone to reduce the size of the video to be uploaded. If the size of the video exceeds 16 MB, WhatsApp takes only the first 16MB of the video and ignores the rest of it. This app splits the video into equal parts of 16MB each and then allows you to upload to WhatsApp or any other app that accepts videos.

### How do I get set up? ###

* Download the source code on Windows.
* Install Visual Studio 2013 or higher.
* Open the solution and build the solution.
* Run it on emulator or install on Windows Phone 8.1 or Windows 10 Mobile.
