﻿using VideoSplitterForWhatsApp.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel.Activation;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.ApplicationModel.DataTransfer;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace VideoSplitterForWhatsApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SplitVideo : BasePage, IFileOpenPickerContinuable
    {

        public SplitVideo()
        {
            this.InitializeComponent();
            this.Loaded += SplitVideo_Loaded;
        }

        private void SplitVideo_Loaded(object sender, RoutedEventArgs e)
        {
            videoSource.Source = _thumbNailColl;
        }

        public async void ContinueFileOpenPicker(FileOpenPickerContinuationEventArgs args)
        {
            if (args.Files.Count > 0)
            {
                Filename = args.Files[0].Path;
                await GetFileProperties();
            }
        }

        private async Task GetFileProperties()
        {
            FileProperties.Inpfile = await Windows.Storage.StorageFile.GetFileFromPathAsync(Filename);
            FileProperties.BasicFileProperties = await FileProperties.Inpfile.GetBasicPropertiesAsync();
            if (FileProperties.Inpfile != null)
            {
                var stream = await FileProperties.Inpfile.OpenAsync(FileAccessMode.Read);
                videoElement.SetSource(stream, FileProperties.Inpfile.ContentType);

                Attach.Visibility = Visibility.Collapsed;
                Split.Visibility = Visibility.Visible; 
            }
        }

        private async void SplitFile_Click(object sender, RoutedEventArgs e)
        {
            videoGrid.Visibility = Visibility.Visible;
            videoElement.Visibility = Visibility.Collapsed;
            Split.Visibility = Visibility.Collapsed;
            Share.Visibility = Visibility.Visible;
            videoElement.Source = null;
            var numfiles = await SplitFile(0, videoElement.NaturalDuration.TimeSpan.TotalSeconds, numFilesTxt);

            if (numfiles > 0)
            {
                OutputText(string.Empty, numFilesTxt);
                OutputText("Please find your files under \"Video Splitter\" in Photos.", numFilesTxt);

                videoGrid.SelectionMode = ListViewSelectionMode.Multiple;
                videoGrid.SelectAll();
                Share.IsEnabled = _thumbNailColl.Any(x => x.Included);
            }
            else
            {
                OutputText(string.Format("{0} is less than 16 MB and doesn't required to be splitted.", FileProperties.Inpfile.Name), Output);
            }
        }

        private void ShareFiles_Click(object sender, RoutedEventArgs e)
        {
            DataTransferManager.ShowShareUI();
        }

        private void videoGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_thumbNailColl.Count > 0)
            {
                foreach (VideoClip item in e.AddedItems)
                {
                    _thumbNailColl[item.Id].Included = true;
                }

                foreach (VideoClip item in e.RemovedItems)
                {
                    _thumbNailColl[item.Id].Included = false;
                }

                Share.IsEnabled = _thumbNailColl.Any(x => x.Included);
            }
        }

        private void AttachFile_Click(object sender, RoutedEventArgs e)
        {
            BrowseThroughFiles();
        }
    }
}
