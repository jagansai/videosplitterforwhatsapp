﻿using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VideoSplitterForWhatsApp.Common;
using Windows.ApplicationModel.DataTransfer;
using Windows.Foundation;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

namespace VideoSplitterForWhatsApp
{


    public sealed class VideoClip : BindableBase
    {
        private BitmapImage _thumbnail;
        public BitmapImage Thumbnail
        {
            get
            {
                return _thumbnail;
            }
            set
            {
                _thumbnail = value;
                SetProperty(() => Thumbnail);
            }
        }
        public string Name { get; set; }
        public int Id { get; set; }
        public StorageFile SplitFile { get; set; }
        public bool Included { get; set; }
    }

    sealed class FileProperties
    {
        private BasicProperties _fileProperties = null;
        private StorageFolder _folder = null;
        private StorageFile _inpfile = null;

        public BasicProperties BasicFileProperties
        {
            get
            {
                return _fileProperties;
            }

            set
            {
                _fileProperties = value;
            }
        }

        public StorageFolder Folder
        {
            get
            {
                return _folder;
            }

            set
            {
                _folder = value;
            }
        }

        public StorageFile Inpfile
        {
            get
            {
                return _inpfile;
            }

            set
            {
                _inpfile = value;
            }
        }
    }



    public class BasePage : Page
    {

        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        protected ObservableCollection<VideoClip> _thumbNailColl = new ObservableCollection<VideoClip>();
        private string _filename = string.Empty;
        private FileProperties _fileProperties = new FileProperties();
        private double _duration = 0.0;
        private readonly double max_secs = 30.0;
        private CancellationTokenSource _cts = null;
        private CoreDispatcher _dispatcher = null;
        private DataTransferManager _dataTransferManager = null;
        private TextBlock _txtBlock = null;


        public BasePage()
        {
            _cts = new CancellationTokenSource();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
            this._dataTransferManager = DataTransferManager.GetForCurrentView();
            _dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(OnDataRequested);
        }

        private void OnDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            GetData(args.Request);
        }

        private void GetData(DataRequest request)
        {
            DataPackage package = request.Data;
            package.Properties.Title = "Sharing Video";
            package.Properties.Description = "I would like to Share a video.Enjoy!";

            DataRequestDeferral deferrel = request.GetDeferral();

            try
            {
                package.SetStorageItems(_thumbNailColl.Where(x => x.Included).Select(x => x.SplitFile));
            }
            finally
            {
                deferrel.Complete();
            }
        }


        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        internal FileProperties FileProperties
        {
            get
            {
                return _fileProperties;
            }

            set
            {
                _fileProperties = value;
            }
        }

        public string Filename
        {
            get
            {
                return _filename;
            }

            set
            {
                _filename = value;
            }
        }

        public TextBlock TxtBlock
        {
            get
            {
                return _txtBlock;
            }

            set
            {
                _txtBlock = value;
            }
        }

        public CancellationTokenSource Cts
        {
            get
            {
                return _cts;
            }

            set
            {
                _cts = value;
            }
        }

        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            
        }


        protected async Task CreateFolder()
        {
            var videoLib = Windows.Storage.KnownFolders.PicturesLibrary;
            _fileProperties.Folder = await videoLib.CreateFolderAsync("Video Splitter", CreationCollisionOption.OpenIfExists);
        }


        protected async Task<double> SplitFile(double start, double duration, TextBlock txtBlock)
        {
            return await SplitFile(start, duration, txtBlock, _thumbNailColl);
        }

        protected async Task<double> SplitFile(double start, double duration, TextBlock txtBlock, ObservableCollection<VideoClip> thumbNailColl)
        {
            _duration = duration - start;
            _txtBlock = txtBlock;
            await CreateFolder();

            double allowed_secs_per_clip = max_secs;
            double numFiles = CalculateNumFilesRequired();
            return await CreateFiles(allowed_secs_per_clip, numFiles, start, _txtBlock, thumbNailColl);
        }


        protected void BrowseThroughFiles()
        {
            Windows.Storage.Pickers.FileOpenPicker picker = new Windows.Storage.Pickers.FileOpenPicker();
            picker.SuggestedStartLocation = Windows.Storage.Pickers.PickerLocationId.VideosLibrary;
            picker.FileTypeFilter.Add(".wmv");
            picker.FileTypeFilter.Add(".mp4");
            picker.PickSingleFileAndContinue();
        }


        private double CalculateNumFilesRequired()
        {
            return System.Math.Max(1.0, Math.Ceiling(_duration / max_secs));
        }



        private async Task<double> CreateFiles(double allowed_secs_per_clip, double numFiles, double start, TextBlock txtBlock, ObservableCollection<VideoClip> thumbNailColl)
        {
            double stop = numFiles == 1 ? (start + _duration) : (double)(start + allowed_secs_per_clip);

            int index = 1;

            var numFilesRemaining = numFiles;
           

            while (numFilesRemaining-- > 0)
            {
                OutputText(string.Format("{0}/{1} file{2} completed", (index - 1), numFiles, (index - 1 == 1 ? "" : "s")), txtBlock);
                thumbNailColl.Add(new VideoClip());
                int fileindex = thumbNailColl.Count - 1;
                thumbNailColl[fileindex].SplitFile = await _fileProperties.Folder.CreateFileAsync(string.Format("VideSplitter_{0}_{1}.mp4", index++, DateTimeOffset.UtcNow.Month),
                    Windows.Storage.CreationCollisionOption.GenerateUniqueName);

                try
                {
                    await TrimClip(start, stop, _fileProperties.Inpfile, fileindex, txtBlock, thumbNailColl);

                    start = stop;
                    stop = (numFilesRemaining == 1) ? (start + _duration) : (start + allowed_secs_per_clip);
                }
                catch (TaskCanceledException)
                {
                    TranscodeError("Transcode Canceled" , txtBlock);
                }
                catch (Exception ex)
                {
                    TranscodeError(ex.Message, txtBlock);
                }
            }
            return numFiles;
        }

        private async Task TrimClip(double start, double stop, StorageFile file, int fileIndex, TextBlock txtBlock, ObservableCollection<VideoClip> thumbNailColl)
        {
            const int ticksToSecs = (1000 * 1000 * 10);
            var profile = Windows.Media.MediaProperties.MediaEncodingProfile.CreateMp4(Windows.Media.MediaProperties.VideoEncodingQuality.Vga);

            var transcoder = new Windows.Media.Transcoding.MediaTranscoder();
            transcoder.TrimStartTime = new TimeSpan((long)start * ticksToSecs);
            transcoder.TrimStopTime = new TimeSpan((long)stop * ticksToSecs);

            transcoder.VideoProcessingAlgorithm = Windows.Media.Transcoding.MediaVideoProcessingAlgorithm.Default;
            var result = await transcoder.PrepareFileTranscodeAsync(_fileProperties.Inpfile, thumbNailColl[fileIndex].SplitFile, profile);

            if (result.CanTranscode)
            {
                var progress = new Progress<double>(TranscodeProgress);
                await result.TranscodeAsync().AsTask(_cts.Token, progress);
                await CreateThumbnail(fileIndex, txtBlock, thumbNailColl);
            }

        }

        private async Task CreateThumbnail(int fileindex, TextBlock txtBlock, ObservableCollection<VideoClip> thumbNailColl)
        {
            string message = string.Empty;
            int retryCount = 0;
            const int maxretrycount = 10;
            do
            {
                ++retryCount;
                try
                {
                    var mediaFile = await Windows.Media.Editing.MediaClip.CreateFromFileAsync(thumbNailColl[fileindex].SplitFile);
                    var comp = new Windows.Media.Editing.MediaComposition();
                    comp.Clips.Add(mediaFile);
                    var stream = await comp.GetThumbnailAsync(new TimeSpan(mediaFile.OriginalDuration.Ticks / 2), 320, 240, Windows.Media.Editing.VideoFramePrecision.NearestKeyFrame);
                    var image = new BitmapImage();
                    image.SetSource(stream);
                    thumbNailColl[fileindex].Thumbnail = image;
                    thumbNailColl[fileindex].Id = fileindex;
                    thumbNailColl[fileindex].Included = true;
                    this.UpdateLayout();
                    retryCount = maxretrycount;
                    message = string.Empty;
                }
                catch (Exception ex)
                {
                    message = ex.Message;
                }
            } while (retryCount < maxretrycount);

            if (message != string.Empty)
            {
                OutputText("One of the files failed to show up here. You could find the videos under \"WhatsApp Video Splitter\"", txtBlock);
            }
        }

        private void TranscodeError(string v, TextBlock txtBlock)
        {
            OutputText(v, txtBlock);
        }


        protected void TranscodeProgress(double percent)
        {
            OutputText("Progress:  " + percent.ToString().Split('.')[0] + "%", _txtBlock);
        }

             


        protected async void OutputText(string text, TextBlock txtBlock)
        {
            _dispatcher = Window.Current.CoreWindow.Dispatcher;
            await _dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                txtBlock.Text = text;
            });
        }

        #region NavigationHelper registration

        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion
    }
}
