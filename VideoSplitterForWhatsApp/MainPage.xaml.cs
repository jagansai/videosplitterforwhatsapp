﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.ApplicationModel.DataTransfer;
using Windows.ApplicationModel.Store;
using Windows.Foundation;
using Windows.Graphics.Display;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=391641

namespace VideoSplitterForWhatsApp
{
    [Windows.Foundation.Metadata.WebHostHidden]
    public abstract class BindableBase : INotifyPropertyChanged
    {
        /// <summary>
        /// Multicast event for property change notifications.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;


        protected bool SetProperty<T>(Expression<Func<T>> propertyExpression)
        {
            this.OnPropertyChanged(ExtractPropertyName(propertyExpression));
            return true;
        }

        protected bool SetProperty(string propertyName)
        {
            this.OnPropertyChanged(propertyName);
            return true;
        }


        private static string ExtractPropertyName<T>(Expression<Func<T>> propertyExpression)
        {
            var memberExp = propertyExpression.Body as MemberExpression;
            if (memberExp == null)
            {
                throw new ArgumentException("Expression must be a MemberExpression.", "propertyExpression");
            }
            return memberExp.Member.Name;
        }


        protected void OnPropertyChanged([CallerMemberName] string propertyName = null)
        {
            var eventHandler = this.PropertyChanged;
            if (eventHandler != null)
            {
                eventHandler(this, new PropertyChangedEventArgs(propertyName));
            }
        }
    }




    public class BooleanToVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                var result = (System.Boolean)value;
                return result;
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }


    public class ImageConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (value != null)
            {
                var stream = (IRandomAccessStream)value;
                var picture = new BitmapImage();
                picture.SetSource(stream);
                return picture;
            }
            return DependencyProperty.UnsetValue;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public class SplitFilesExistConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            try
            {
                if (value != null)
                {
                    var data = value as CollectionViewSource;
                    return (data.Source as ObservableCollection<VideoClip>).Any(x => x.Included);
                }
            }
            catch (Exception)
            {
            }
            return false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }


    public class ProgressBarVisibilityConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            var mediaElement = (MediaElement)value;
            return (mediaElement.Source != null);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotImplementedException();
        }
    }

    public enum NotifyType
    {
        StatusMessage,
        ErrorMessage
    };


    public enum States
    {
        ChooseVideo,
        VideoChoosen,
        CustomTrim,
        SplitVideo
    }


    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        public static MainPage current = null;
        //CancellationTokenSource _cts = null;
        Windows.UI.Core.CoreDispatcher _dispatcher = Window.Current.Dispatcher;

        public static MainPage Current
        {
            get
            {
                return current;
            }
        }

        public MainPage()
        {
            this.InitializeComponent();
            current = this;
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

            this.Loaded += MainPage_Loaded;
            this.NavigationCacheMode = NavigationCacheMode.Required;
        }



        private void MainPage_Loaded(object sender, RoutedEventArgs e)
        {
            hamburger.IsLeftPaneOpen = false;
        }

        protected async override void OnNavigatedTo(NavigationEventArgs e)
        {
            var licenseInfo = CurrentApp.LicenseInformation;
            if (licenseInfo.IsTrial)
            {
                try
                {
                    var result = await CurrentApp.RequestAppPurchaseAsync(false);
                    if (licenseInfo.IsActive)
                    {
                        OutputText("Thanks for the purchase! Enjoy sharing the videos.", Output);
                    }
                    else
                    {
                        OutputText("The purchase wasn't successful :`-(", Output);
                        showMe.Visibility = Visibility.Visible;
                    }
                }
                catch (Exception)
                {
                    OutputText("The purchase wasn't successful :`-(", Output);
                    showMe.Visibility = Visibility.Visible;
                }
            }
            else
            {

                const string stackcook = "stackcook";
                bool deferred_decision = ApplicationSettings(stackcook, true);

                if (deferred_decision)
                {
                    var md = new Windows.UI.Popups.MessageDialog("Are you a developer? Would like to try an app for StackOverflow ? Try StackCook.", "Try StackCook");
                    bool? reviewresult = null;
                    md.Commands.Add(new Windows.UI.Popups.UICommand("Interested", new Windows.UI.Popups.UICommandInvokedHandler((cmd) => reviewresult = true)));
                    md.Commands.Add(new Windows.UI.Popups.UICommand("Later", new Windows.UI.Popups.UICommandInvokedHandler((cmd) => reviewresult = false)));
                    await md.ShowAsync();
                    if (reviewresult == true)
                    {
                        await Windows.System.Launcher.LaunchUriAsync(new Uri(string.Format("ms-windows-store:navigate? appid={0}", "9c34fd1b-a44f-4b5b-8932-c7c63db66bcb")));
                        // save decision.
                        ApplicationSettings(stackcook, false);
                    }
                    else
                    {
                        ApplicationSettings(stackcook, true);
                    }
                }
            }
        }

        private T ApplicationSettings<T>(string key, T value)
        {
            var settings = ApplicationData.Current.LocalSettings;
            if (!settings.Values.Keys.Contains(key))
            {
                settings.Values.Add(key, value);
            }
            else
            {
                value = (T)settings.Values[key];
            }
            return value;
        }

        private static int GetUseCount(ApplicationDataContainer settings)
        {
            int started = 0;
            if (settings.Values.Keys.Contains("started"))
            {
                started = (int)settings.Values["started"];
                settings.Values["started"] = ++started;
            }
            else
            {
                settings.Values.Add("started", ++started);
            }

            return started;
        }

   

        private void ShareFiles_Click(object sender, RoutedEventArgs e)
        {
            DataTransferManager.ShowShareUI();
        }

        private async void ReviewButton_Click(object sender, RoutedEventArgs e)
        {
            ApplicationData.Current.LocalSettings.Values["reviewed"] = true;
            await Windows.System.Launcher.LaunchUriAsync(new Uri(string.Format("ms-windows-store:reviewapp?appid={0}", CurrentApp.AppId)));
        }



        private void TrimVideo_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(TrimVideo));
        }

        private void SplitVideo_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SplitVideo)); 
        }

        private async void OutputText(string text, TextBlock txtBlock)
        {
            _dispatcher = Window.Current.CoreWindow.Dispatcher;
            await _dispatcher.RunAsync(Windows.UI.Core.CoreDispatcherPriority.Normal, () =>
            {
                txtBlock.Text = text;
            });
        }

        private async void contactButton_Click(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(new System.Uri("https://www.facebook.com/jagan.sai"));
        }

        private async void demoButton_Click(object sender, RoutedEventArgs e)
        {
            await Windows.System.Launcher.LaunchUriAsync(new System.Uri("https://www.youtube.com/channel/UCWChMYxJuc7qklGkNFdfpaw/videos?view_as=subscriber"));
        }
        private void StichClips_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(StichVideo));
        }
    }
}
