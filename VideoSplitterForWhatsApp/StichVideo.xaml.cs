﻿using VideoSplitterForWhatsApp.Common;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel.Activation;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.ApplicationModel.DataTransfer;
using Windows.Media.Editing;

// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace VideoSplitterForWhatsApp
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>

    public sealed partial class StichVideo : BasePage, IFileOpenPickerContinuable
    {

        private ObservableCollection<VideoClip> _clipsThumbNail = new ObservableCollection<VideoClip>();
        private TimeSpan _startTs;
        private TimeSpan _endTs;

        public StichVideo()
        {
            this.InitializeComponent();
            clipsSource.Source = _clipsThumbNail;
            videoSource.Source = _thumbNailColl;
        }

        private async void StichVideo_Click(object sender, RoutedEventArgs e)
        {
            StichVideoState();
            var composition = new MediaComposition();

            for (int i = 0; i < _clipsThumbNail.Count; ++i)
            {
                composition.Clips.Add(await MediaClip.CreateFromFileAsync(_clipsThumbNail[i].SplitFile));
            }

            var file = await FileProperties.Folder.CreateFileAsync(string.Format("VideSplitter_{0}_{1}.mp4", _clipsThumbNail.Count, DateTimeOffset.UtcNow.Month), CreationCollisionOption.GenerateUniqueName);
            var profile = Windows.Media.MediaProperties.MediaEncodingProfile.CreateMp4(Windows.Media.MediaProperties.VideoEncodingQuality.Vga);
            var result = composition.RenderToFileAsync(file, MediaTrimmingPreference.Precise, profile);
            TxtBlock = numFilesTxt;
            var progress = new Progress<double>(TranscodeProgress);
            await result.AsTask(Cts.Token, progress);
            Filename = file.Path;
            await GetFileProperties();
            //Split.IsEnabled = true;
            videoElement.Visibility = Visibility.Visible;
            Share.Visibility = Visibility.Visible;
            Share.IsEnabled = true;
        }



        public async void ContinueFileOpenPicker(FileOpenPickerContinuationEventArgs args)
        {
            if (args.Files.Count > 0)
            {
                Filename = args.Files[0].Path;
                await GetFileProperties();
                ChooseVideoState();
            }
        }

        private async Task GetFileProperties()
        {
            FileProperties.Inpfile = await Windows.Storage.StorageFile.GetFileFromPathAsync(Filename);
            FileProperties.BasicFileProperties = await FileProperties.Inpfile.GetBasicPropertiesAsync();
            if (FileProperties.Inpfile != null)
            {
                var stream = await FileProperties.Inpfile.OpenAsync(FileAccessMode.Read);
                videoElement.SetSource(stream, FileProperties.Inpfile.ContentType);
            }
        }

        private async void AddClips_Click(object sender, RoutedEventArgs e)
        {
            videoElement.Source = null;
            clipsGrid.Visibility = Visibility.Visible;
            Add.IsEnabled = false;
            await SplitFile(_startTs.TotalSeconds, _endTs.TotalSeconds, numFilesTxt, _clipsThumbNail);
            Stich.Visibility = HasMoreThanOneClip() ? Visibility.Visible : Visibility.Collapsed;
            Add.IsEnabled = true;
            await GetFileProperties();

            videoElement.MediaOpened += (source, args) =>
            {
                var mediaElement = (MediaElement)source;
                if (mediaElement.CanSeek)
                {
                    mediaElement.Position = _endTs;
                }
            };

            
        }

        private bool HasMoreThanOneClip()
        {
            return _clipsThumbNail.Count > 1;
        }

        private void videoGrid_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (_thumbNailColl.Count > 0)
            {
                foreach (VideoClip item in e.AddedItems)
                {
                    _thumbNailColl[item.Id].Included = true;
                }

                foreach (VideoClip item in e.RemovedItems)
                {
                    _thumbNailColl[item.Id].Included = false;
                }

                Share.IsEnabled = _thumbNailColl.Any(x => x.Included);
            }
        }

        private void AttachFile_Click(object sender, RoutedEventArgs e)
        {
            BrowseThroughFiles();
        }

        private async void ShareFiles_Click(object sender, RoutedEventArgs e)
        {
            videoGrid.Visibility = Visibility.Visible;
            videoElement.Visibility = Visibility.Collapsed;
            //Split.Visibility = Visibility.Collapsed;
            videoElement.Source = null;
            var numfiles = await SplitFile(0, videoElement.NaturalDuration.TimeSpan.TotalSeconds, numFilesTxt, _thumbNailColl);

            if (numfiles > 0)
            {
                OutputText(string.Empty, numFilesTxt);
                OutputText("Please find your files under \"Video Splitter\" in Photos.", numFilesTxt);

                videoGrid.SelectionMode = ListViewSelectionMode.Multiple;
                videoGrid.SelectAll();
                Share.IsEnabled = _thumbNailColl.Any(x => x.Included);
            }
            else
            {
                OutputText(string.Format("{0} is less than 16 MB and doesn't required to be splitted.", FileProperties.Inpfile.Name), Output);
            }
            DataTransferManager.ShowShareUI();
        }

        private void endButton_Click(object sender, RoutedEventArgs e)
        {
            if (videoElement.Position > _startTs)
            {
                endText.Text = string.Format("{0:hh\\:mm\\:ss}", videoElement.Position);
                _endTs = videoElement.Position;
                Add.IsEnabled = true;
            }
            else
            {
                Add.IsEnabled = false;
            }
        }

        private void startButton_Click(object sender, RoutedEventArgs e)
        {
            startText.Text = string.Format("{0:hh\\:mm\\:ss}", videoElement.Position);
            _startTs = videoElement.Position;
            endText.Text = string.Format("{0:hh\\:mm\\:ss}", videoElement.NaturalDuration.TimeSpan);
            _endTs = videoElement.NaturalDuration.TimeSpan;
            Add.IsEnabled = true;
        }

        private async void SplitFile_Click(object sender, RoutedEventArgs e)
        {
            videoGrid.Visibility = Visibility.Visible;
            videoElement.Visibility = Visibility.Collapsed;
            //Split.Visibility = Visibility.Collapsed;
            Share.Visibility = Visibility.Visible;
            videoElement.Source = null;
            var numfiles = await SplitFile(0, videoElement.NaturalDuration.TimeSpan.TotalSeconds, numFilesTxt, _thumbNailColl);

            if (numfiles > 0)
            {
                OutputText(string.Empty, numFilesTxt);
                OutputText("Please find your files under \"Video Splitter\" in Photos.", numFilesTxt);

                videoGrid.SelectionMode = ListViewSelectionMode.Multiple;
                videoGrid.SelectAll();
                Share.IsEnabled = _thumbNailColl.Any(x => x.Included);
            }
            else
            {
                OutputText(string.Format("{0} is less than 16 MB and doesn't required to be splitted.", FileProperties.Inpfile.Name), Output);
            }
        }


        private void ChooseVideoState()
        {
            StartPanel.Visibility = Visibility.Visible;
            EndPanel.Visibility = Visibility.Visible;
            Attach.Visibility = Visibility.Collapsed;
            Add.Visibility = Visibility.Visible;
        }

        private void StichVideoState()
        {
            videoElement.Source = null;
            StartPanel.Visibility = Visibility.Collapsed;
            EndPanel.Visibility = Visibility.Collapsed;
            Attach.Visibility = Visibility.Collapsed;
            Add.Visibility = Visibility.Collapsed;
            Stich.Visibility = Visibility.Collapsed;
            clipsGrid.Visibility = Visibility.Collapsed;
            //Split.Visibility = Visibility.Visible;
            Share.Visibility = Visibility.Visible;
            videoElement.Visibility = Visibility.Collapsed;
        }
    }
}
